@echo off
setlocal

set outputdir="..\..\artifacts"

dotnet publish TestAssignment.sln --output %outputdir%

endlocal