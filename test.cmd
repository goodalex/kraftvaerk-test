@echo off
setlocal

set outputdir="artifacts"
set testassemblypath="%outputdir%\TestAssignment.dll"
set logoutputdir=%outputdir%

if not exist %testassemblypath% (
    call build.cmd
)

dotnet vstest %testassemblypath% --logger:"nunit;LogFilePath=%logoutputdir%\TestResults.xml"

endlocal