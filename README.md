# kraftvaerk-test

Test assignment for QA‎ Automation Engineer job in Kraftvaerk. Includes a simple Selenium test against the Google website, based on .NET Core 2.1, NUnit 3, and a custom framework. The test may be run from a command line on a Windows system.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The test requires __.NET Core 2.1 SDK__ which may be downloaded from the [Microsoft site](https://www.microsoft.com/net/download/dotnet-core/2.1). Both x86 and x64 editions will do, based on a system this test is going to be run.

The test uses __Google Chrome__ browser (download from [here](https://www.google.com/chrome/)). The last verified build for the test was 70.0.3538.102.

The test was verified to be compatible with Windows 7 x86 and Windows 10 x64.

### Installation

For .NET Core and Google Chrome installers, walk through the steps as usual. Reboot is not required.

The test may then be run locally in the folder where this repository is cloned.

## Running the tests

Simply execute `test.cmd` in the root folder of the repository. The project will automatically be built prior to running.

By default, the built files go to `artifacts` sub-folder. This may be changed in `build.cmd` and `test.cmd` files, just edit the `outputdir` variable in both of them. If you use the relative paths, keep in mind that current directory for `build.cmd` is the project folder, and for the `test.cmd` it's the root of the repository.

There is also a `logoutputdir` variable in the `test.cmd` for choosing a place to write a test log (by default, NUnit 3 XML format of the log is used).

## Deployment

Since the project is tiny, for convenience it is by default published rightaway into `artifacts` sub-folder, instead of just being built. To deploy the test on a remote machine (that doesn't have this repository cloned), create a folder there in an accessible location (like My Documents folder), and then simply move there all the contents of the `artifacts` sub-folder.

Then, inside the folder, there is already a `Run.cmd` file. Simply launch it, and the test should pass.

The test log also goes to the same folder by default, which may be changed in the `Run.cmd` file.

## Built With

* [Selenium WebDriver](https://www.seleniumhq.org/download/)
* [NUnit 3](https://github.com/nunit/nunit/releases/latest)

## Authors

* **Aleksandr Polianskii** - [GoodAlex](https://bitbucket.org/goodalex/)

## License

This project is private and currently copyrighted to Aleksandr Polianskii, with no public license available.