﻿namespace TestAssignment
{
    using System;
    using System.Drawing;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    public sealed class Driver
    {
        public const int WindowWidth = 1366;
        public const int WindowHeight = 768;

        public const int ImplicitTimeout = 2;
        public const int ExplicitTimeout = 5;

        private static IWebDriver instance;

        public static IWebDriver Current
        {
            get
            {
                if (instance == null)
                {
                    throw new NullReferenceException("No driver is currently running. Please call the Run() method first");
                }

                return instance;
            }
        }

        public static void Run()
        {
            Quit();

            instance = new ChromeDriver(Environment.CurrentDirectory);

            instance.Manage().Window.Position = new Point(0, 0);
            instance.Manage().Window.Size = new Size(WindowWidth, WindowHeight);

            instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(ImplicitTimeout);
        }

        public static void Quit()
        {
            if (instance != null)
            {
                instance.Quit();
                Waiter.Reset();
            }
        }
    }
}
