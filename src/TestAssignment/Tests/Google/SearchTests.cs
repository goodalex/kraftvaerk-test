﻿namespace TestAssignment.Tests.Google
{
    using NUnit.Framework;

    using TestAssignment.Pages.Google;

    [TestFixture]
    public class SearchTests : BaseTests
    {
        protected SearchPage SearchPage { get; set; }

        protected ResultsPage ResultsPage { get; set; }

        [Test]
        public void SearchTest()
        {
            const string SiteToSearch = "yandex.ru";

            this.SearchPage.NavigateTo();
            this.SearchPage.Search(SiteToSearch);
            this.ResultsPage.WaitForAtLeastOneResult();
            Assert.True(this.ResultsPage.GetFirstResultLink().Contains(SiteToSearch));
        }
    }
}
