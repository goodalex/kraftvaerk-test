﻿namespace TestAssignment.Tests
{
    using NUnit.Framework;

    using TestAssignment.Helpers;

    public abstract class BaseTests
    {
        protected BaseTests()
        {
            Initializer.InitializePages(this);
        }

        [SetUp]
        public void SetUp()
        {
            Driver.Run();
        }

        [TearDown]
        public void TearDown()
        {
            Driver.Quit();
        }
    }
}
