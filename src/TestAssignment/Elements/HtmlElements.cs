﻿namespace TestAssignment.Elements
{
    using System.Collections.ObjectModel;

    using OpenQA.Selenium;

    public class HtmlElements : BaseHtmlElement
    {
        public ReadOnlyCollection<IWebElement> Find()
        {
            return Driver.Current.FindElements(this.SearchBy);
        }

        public ReadOnlyCollection<IWebElement> FindIn(IWebElement parent)
        {
            return parent.FindElements(this.SearchBy);
        }
    }
}
