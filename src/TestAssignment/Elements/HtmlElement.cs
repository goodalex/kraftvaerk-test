﻿namespace TestAssignment.Elements
{
    using OpenQA.Selenium;

    public class HtmlElement : BaseHtmlElement
    {
        public IWebElement Find()
        {
            return Driver.Current.FindElement(this.SearchBy);
        }

        public IWebElement FindIn(IWebElement parent)
        {
            return parent.FindElement(this.SearchBy);
        }
    }
}
