﻿namespace TestAssignment.Pages.Google
{
    using TestAssignment.Attributes;
    using TestAssignment.Elements;

    public class ResultsPage : BasePage
    {
        [FindBy(Css = "div.r")]
        protected HtmlElements Results { get; set; }

        [FindBy(Css = "a[href]")]
        protected HtmlElement Link { get; set; }

        public void WaitForAtLeastOneResult()
        {
            Waiter.Current.Until(driver => this.Results.Find().Count > 1);
        }

        public string GetFirstResultLink()
        {
            return this.Link.FindIn(this.Results.Find()[0]).GetAttribute("href");
        }
    }
}
