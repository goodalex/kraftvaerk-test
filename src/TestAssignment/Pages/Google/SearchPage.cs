﻿namespace TestAssignment.Pages.Google
{
    using TestAssignment.Attributes;
    using TestAssignment.Elements;

    [Page(Url = "https://google.com")]
    public class SearchPage : BasePage
    {
        [FindBy(Css = "input[name=q]")]
        protected HtmlElement SearchField { get; set; }

        [FindBy(Css = "div[jscontroller] div[jsname] input[name=btnK]")]
        protected HtmlElement DropdownSearchButton { get; set; }

        public void Search(string text)
        {
            this.SearchField.Find().SendKeys(text);
            Waiter.Current.Until(driver => this.DropdownSearchButton.Find().Displayed);
            this.DropdownSearchButton.Find().Click();
        }
    }
}
