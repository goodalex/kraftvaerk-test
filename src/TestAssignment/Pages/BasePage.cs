﻿namespace TestAssignment.Pages
{
    using TestAssignment.Attributes;
    using TestAssignment.Helpers;

    public abstract class BasePage
    {
        protected BasePage()
        {
            PageAttribute.Apply(this);
            Initializer.InitializeElements(this);
        }

        public string Url { get; protected set; }

        public void NavigateTo()
        {
            Driver.Current.Url = this.Url;
        }
    }
}
