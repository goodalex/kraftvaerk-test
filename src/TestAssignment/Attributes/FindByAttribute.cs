﻿namespace TestAssignment.Attributes
{
    using System;
    using System.Reflection;

    using OpenQA.Selenium;

    using TestAssignment.Elements;

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class FindByAttribute : Attribute
    {
        private By searchBy;

        public string Css
        {
            get => null;
            set => this.searchBy = By.CssSelector(value);
        }

        public string Class
        {
            get => null;
            set => this.searchBy = By.ClassName(value);
        }

        public string Id
        {
            get => null;
            set => this.searchBy = By.Id(value);
        }

        public string Link
        {
            get => null;
            set => this.searchBy = By.LinkText(value);
        }

        public string Name
        {
            get => null;
            set => this.searchBy = By.Name(value);
        }

        public string PartialLink
        {
            get => null;
            set => this.searchBy = By.PartialLinkText(value);
        }

        public string Tag
        {
            get => null;
            set => this.searchBy = By.TagName(value);
        }

        public string XPath
        {
            get => null;
            set => this.searchBy = By.XPath(value);
        }

        public static void Apply(object obj, PropertyInfo property)
        {
            var attribute = property.GetCustomAttribute<FindByAttribute>(false);
            if (attribute == null)
            {
                return;
            }

            const BindingFlags PropertyFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var searchByProperty = typeof(BaseHtmlElement).GetProperty("SearchBy", PropertyFlags);
            if (searchByProperty == null)
            {
                throw new NullReferenceException("There is no SearchBy property in BaseHtmlElement class. Was it renamed?");
            }

            var element = (BaseHtmlElement)property.GetValue(obj);
            searchByProperty.SetValue(element, attribute.searchBy);
        }
    }
}
