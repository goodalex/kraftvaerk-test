﻿namespace TestAssignment.Attributes
{
    using System;
    using System.Reflection;

    using TestAssignment.Pages;

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class PageAttribute : Attribute
    {
        public string Url { get; set; }

        public static void Apply(BasePage page)
        {
            var attribute = page.GetType().GetCustomAttribute<PageAttribute>(false);
            if (attribute == null)
            {
                return;
            }

            const BindingFlags PropertyFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var urlProperty = typeof(BasePage).GetProperty("Url", PropertyFlags);
            if (urlProperty == null)
            {
                throw new NullReferenceException("There is no Url property in BasePage class. Was it renamed?");
            }

            urlProperty.SetValue(page, attribute.Url);
        }
    }
}
