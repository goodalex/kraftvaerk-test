﻿namespace TestAssignment
{
    using System;

    using OpenQA.Selenium.Support.UI;

    public sealed class Waiter
    {
        private static WebDriverWait instance;

        public static WebDriverWait Current
        {
            get
            {
                if (instance == null)
                {
                    return new WebDriverWait(Driver.Current, TimeSpan.FromSeconds(Driver.ExplicitTimeout));
                }

                return instance;
            }
        }

        public static void Reset()
        {
            instance = null;
        }
    }
}
