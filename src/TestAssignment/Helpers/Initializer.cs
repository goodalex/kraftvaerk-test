﻿namespace TestAssignment.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using TestAssignment.Attributes;
    using TestAssignment.Elements;
    using TestAssignment.Pages;

    public static class Initializer
    {
        public static void InitializePages(object obj)
        {
            GetInitializedProperties(obj, typeof(BasePage));
        }

        public static void InitializeElements(object obj)
        {
            var properties = GetInitializedProperties(obj, typeof(BaseHtmlElement));

            foreach (var property in properties)
            {
                FindByAttribute.Apply(obj, property);
            }
        }

        private static List<PropertyInfo> GetInitializedProperties(object obj, Type type)
        {
            const BindingFlags PropertyFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var properties = obj.GetType().GetProperties(PropertyFlags)
                .Where(property => property.CanWrite
                                   && !property.PropertyType.IsAbstract
                                   && type.IsAssignableFrom(property.PropertyType)).ToList();

            foreach (var property in properties)
            {
                if (property.GetValue(obj) != null)
                {
                    continue;
                }

                var newValue = Activator.CreateInstance(property.PropertyType);
                property.SetValue(obj, newValue);
            }

            return properties;
        }
    }
}
